
from django.contrib import admin
from django.urls import path
from .views import home_view,about_view,contact_view,article_view,ExpenseListView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',home_view),
    path('about/',about_view),
    path('contact-us/',contact_view),
    path('article-list/',article_view),

    path('expense-list/', ExpenseListView.as_view())
]
