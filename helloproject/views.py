import re
from django.shortcuts import render, redirect
from django.views import View

article_list = [
    {
        "title": "First article",
        "content": "This is the first article",
        "date": "2022-1-2"
    },
    {
        "title": "Second article",
        "content": "This is the Second article",
        "date": "2022-1-3"
    },
    {
        "title": "Third article",
        "content": "This is the Third article",
        "date": "2022-1-4"
    },
    {
        "title": "Fourth article",
        "content": "This is the Fourth article",
        "date": "2022-1-5"
    },
]

def home_view(request):
    context = {
        'name': 'Nishma kafle',
        "age": 23,
        "address":"Gwarko",
        "about_job": {
            'company':'Infodevelopers',
            'location':'sanepa',
        },
        'intrests':['progamming','learning','travelling']
    }

    return render(request,'home.html',context)


def about_view(request):
    if request.method == "GET":
        name =request.GET.get('title')
        desc = request.GET.get('description')
        print(name, desc,"???????")
        return render(request,'about.html')

    else:
        print("This is Post Request")
        name =request.POST.get('title')
        desc = request.POST.get('description')
        print(name, desc,"???????")
        return redirect("/")

def contact_view(request):
    return render(request,'contact.html')


def article_view(request):
    context={
        'article_list':article_list
    }
    return render(request,'article.html',context)


expenses =  [
    {
        "title": "Movie Watching",
        "amount":50
    },
    {
        "title": 'Travelling',
        "amount":1000
    }
]

class ExpenseListView(View):
    def get(self,request):
        context = {
            "expenses": expenses
            
        }
        return render(request,'expense_list.html',context)

    def post(self, request):
        title = request.POST.get("title")
        amount = request.POST.get('amount')
        print("Title:", title)
        print("Amount: ",amount)
        new_expense = {
            "title": title,
            "amount": amount
        }
        expenses.append(new_expense)


        return redirect('/expense-list/')
